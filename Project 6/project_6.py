# install the libraries – see: http://biom-format.org/
!pip install biom-format
!pip install h5py
!pip install fbprophet
!wget https://github.com/twbattaglia/MicrobeDS/raw/master/data-raw/MovingPictures/67_otu_table.biom
!wget https://raw.githubusercontent.com/twbattaglia/MicrobeDS/master/data-raw/MovingPictures/550_prep_72_qiime_20161216-085930.txt

# will disable "future warnings"
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

import pandas as pd
import numpy as np

from fbprophet import Prophet
from fbprophet.plot import add_changepoints_to_plot
import biom

biom_data = biom.load_table('67_otu_table.biom')
meta_df = pd.read_csv('550_prep_72_qiime_20161216-085930.txt', sep='\t')

# convert the biom data to a data-frame
# this might takes some minutes for larger files
biom_df = biom_data.to_dataframe()
biom_df = biom_df.transpose()
biom_df.index.names = ['#SampleID']
print(biom_df.head())

df = pd.merge(left=biom_df, 
              right=meta_df, 
              on='#SampleID')
df['ds'] = pd.to_datetime(df['collection_timestamp'])

print(df.head())
print(df.groupby('host').size())
print(df.groupby('body_site').size())
print(df.groupby('common_sample_site').size())


male = df.loc[df['host'] == 'M3']

# from samples from males, select samples from feces
male_feces = male.loc[male['common_sample_site'] == 'feces']
otu_list = ['359105', '4371046', '1081058']
for otu in otu_list:
    print(otu, 'sum:', male_feces[otu].sum())

data = male_feces[['4371046', 'ds']]
data.rename(columns={'4371046': 'y'}, 
            inplace=True)

data.sort_values('ds', axis=0, ascending=True, inplace=True)
data = data.to_dense()
print(data.head())

# train with additive seasonality
model_add = Prophet()
model_add.fit(data)

# train with multiplicative seasonality
model_multi = Prophet(seasonality_mode='multiplicative')
model_multi.fit(data)

# make future for both seasonalities
future_add = model_add.make_future_dataframe(freq='d', 
                                             periods=60)
future_multi = model_multi.make_future_dataframe(freq='d', 
                                                 periods=60)

# forcast for additive 
forecast_add = model_add.predict(future_add)
print(
    forecast_add[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail())

#forcast for multiactive 
forecast_multi = model_multi.predict(future_multi)
print(
    forecast_multi[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail())

# plot change point for additive
fig_add = model_add.plot(forecast_add)
a = add_changepoints_to_plot(
    fig_add.gca(), model_add, forecast_add)
fig_add_com = model_add.plot_components(forecast_add)

import matplotlib
get_ipython().run_line_magic('matplotlib', 'inline')
matplotlib.rcParams['figure.figsize'] = [12.0, 8.0]
!wget https://raw.githubusercontent.com/AileenNielsen/TimeSeriesAnalysisWithPython/master/data/AirPassengers.csv

# load dataset and reformat
df = pd.read_csv('AirPassengers.csv')
df['Month'] = pd.to_datetime(df['Month'],  
                             format='%Y-%m-%d')
print(df.isnull().sum())

# rename columns as required in Prophet
column_name_1, column_name_2 = df.columns
df.rename(
    columns={column_name_1: 'ds', column_name_2: 'y'},
    inplace=True)
print(df.head())

# additive seasonality
model = Prophet().fit(df)

# predict and plot
future = model.make_future_dataframe(periods=10, freq='M')
forecast = model.predict(future)
fig1 = model.plot(forecast)

# plot components
fig2 = model.plot_components(forecast)
print(fig2)

# change-point analysis
from fbprophet.plot import add_changepoints_to_plot

fig = model.plot(forecast)
print(add_changepoints_to_plot(fig.gca(), model, forecast))

# multiplicable seasonality
model = Prophet(seasonality_mode='multiplicative').fit(df)

# predict and plot
future = model.make_future_dataframe(periods=10, freq='M')
forecast = model.predict(future)
fig3 = model.plot(forecast)
print(fig3)

fig4 = model.plot_components(forecast)
print(fig4)

fig = model.plot(forecast)
print(add_changepoints_to_plot(fig.gca(), model, forecast))

# remove outliers on multiplicative
m = Prophet(seasonality_mode='multiplicative').fit(df)
fig = m.plot(m.predict(future))