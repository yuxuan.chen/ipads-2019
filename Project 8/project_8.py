from IPython import get_ipython

from google.colab import drive
drive.mount('/content/drive')

from google.colab import auth
auth.authenticate_user()
print('Authenticated')

get_ipython().run_line_magic('load_ext', 'google.colab.data_table')
project_id = 'tutorial-258411'
get_ipython().run_line_magic('env', 'GCLOUD_PROJECT=tutorial-258411')

from google.cloud import bigquery
client = bigquery.Client(project=project_id)
import pandas as pd
import numpy as np
from pandas import DataFrame
import time
import os, json

!pip install fbprophet
from fbprophet import Prophet
!pip install pingouin
import pingouin as pg

stu_path = '/content/drive/My Drive/Colab Notebooks/data/studentLife'


# %%
def get_json_list(attr='Stress'):
    # get path by concatenation
    dir_attr = os.path.join(stu_path, 'EMA', 'response', attr)

    # get and sort the filename list
    json_list = [pos_json for pos_json in os.listdir(dir_attr) \
        if pos_json.endswith('.json')]
    json_list.sort()
    return json_list


def get_csv_list(attr='conversation'):
    dir_attr = os.path.join(stu_path, 'sensing', attr)

    # get and sort the filename list
    csv_list = [pos_json for pos_json in os.listdir(dir_attr) \
        if pos_json.endswith('.csv')]
    csv_list.sort()
    return csv_list


# Load Stress Data
# 1. Get direction path and use a filter to get all the `.json` files in this directory.
# 2. Read `.json` files to Pandas dataframe according to the sorted filename list 
# generated in step 1.
# 3. Kick out the files which do not contain even `resp_time` and `level` information. 
# We find that `u13` should be rejected.
# 4. For each qualified (e.g. with `resp_time` and `level` columns) file, take these 
# columns and add a new column called `student_id` taken from the filename.
# 5. Instead of manually deleting the useless rows at the beginning of the file, we 
# remove such rows by `df.dropna()`.

stress_df = []
json_stress_list = get_json_list('Stress')

# for file_name in json_files:
for file_name in json_stress_list:
    # read json
    df = pd.read_json(
        os.path.join(stu_path, 'EMA/response/Stress', file_name),
        dtype={'level': int})

    # only accept .json files containing the two columns we require   
    if 'resp_time' in df.columns.to_list() and 'level' in df.columns.to_list(): 
        # accept this file
        print(file_name, df.columns.to_list(), '=' * 5 + '>', 'accepted')

        # reformat column names
        df.columns = df.columns.str.replace(' ', '_')
        
        # reformat timestamp
        df['resp_time'] = pd.to_datetime(df['resp_time'], unit='s')
        df['date'] = df['resp_time'].dt.date
        df['time'] = df['resp_time'].dt.time

        # add student_id from filename
        student_id = file_name[7: 10]
        df.insert(0, 'student_id', student_id)

        # drop the rows with NaN in 'level' or 'resp_time'
        df.dropna(subset = ['resp_time', 'level'], inplace=True)
        
        # take only the columns we need
        df = df[['student_id', 'date', 'time', 'level']]
        stress_df.append(df)
    
    else: # reject this file
        print(file_name, df.columns.to_list(), '=' * 5 + '>', 'rejected')

# concatenate all the .json files into one big table
stress_table = pd.concat(stress_df)
print(stress_table.head().to_string(index=False))


# Load Activity Data
# 1. Get direction path and use a filter to get all the `.json` files in this directory.
# 2. Read `.json` files to Pandas dataframe.
# 3. Kick out the files which do not contain 4 activity columns. We find that `u05`, 
# `u09`, `u13` should be rejected.
# 4. For each qualified file, take required columns and add a new column called 
# `student_id`, similarly done as above.
# 5. Remove rows with NaN by `df.dropna()`.

activity_df = [] # a list storing the df of each qualified .json file
json_activity_list = get_json_list('Activity')

for file_name in json_activity_list:
    # read json
    df = pd.read_json(
        os.path.join(stu_path, 'EMA/response/Activity', file_name))

    # only accept .json files containing the columns we require   
    if len(df.columns.to_list()) > 5: 
        # reformat column names
        df.columns = df.columns.str.replace(' ', '_')
        
        # accept this file
        print(file_name, df.columns.to_list(), '=' * 5 + '>', 'accepted')
        
        # reformat timestamp "resp_time" into date
        df['resp_time'] = pd.to_datetime(df['resp_time'], unit='s')
        df['date'] = df['resp_time'].dt.date
        df['time'] = df['resp_time'].dt.time
        
        # take only the columns we need
        columns_required = ['date', 'time', 'relaxing', 'working', \
            'other_relaxing', 'other_working']
        df = df[columns_required]

        # add student_id from filename
        student_id = file_name[9: 12]
        df.insert(0, 'student_id', student_id)

        # in each accepted file, drop the rows with NaN
        df.dropna(subset = columns_required, inplace=True)
        activity_df.append(df)
    
    else: # reject this file
        print(file_name, df.columns.to_list(), '=' * 5 + '>', 'rejected')

# concatenate all the .json files into one big table
activity_table = pd.concat(activity_df)
print(activity_table.head().to_string(index=False))


# Load Sensing Activity Data
sen_act_df = [] # a list storing the df of each qualified .json file
csv_sen_act_list = get_csv_list('activity')

for file_name in csv_sen_act_list:
    # read csv
    df = pd.read_csv(
        os.path.join(stu_path, 'sensing/activity', file_name))

    # only accept .json files containing the columns we require   
    if len(df.columns.to_list()) > 1: 
        # reformat column names
        df.columns = df.columns.str.replace(' ', '_')
        df.rename(columns={'_activity_inference': 'activity_level'}, 
                  inplace=True)
        
        # reformat timestamp into date
        df['timestamp'] = pd.to_datetime(df['timestamp'], unit='s')
        df['date'] = df['timestamp'].dt.date
        df['time'] = df['timestamp'].dt.time
        
        # take only the columns we need
        columns_required = ['date', 'time', 'activity_level']
        df = df[columns_required]

        # in each accepted file, drop the rows with NaN
        df.dropna(subset = columns_required, inplace=True)

        # group by sum of date
        df = df.groupby(
            by = ['date'], 
            as_index=False).agg({'activity_level': 'sum'})

        # add student_id from filename
        student_id = file_name[9: 12]
        df.insert(0, 'student_id', student_id)     
        
        sen_act_df.append(df)
    
    else: # reject this file
        print(file_name, df.columns.to_list(), '=' * 5 + '>', 'rejected')

# concatenate all the .json files into one big table
sen_act_table = pd.concat(sen_act_df)
print(sen_act_table.head().to_string(index=False))


# Import the three tables to GBQ
# load stress table
start = time.time()
stress_table.to_gbq(
    'StudentLife.Stress',
    project_id,
    chunksize=10000, 
    if_exists='replace', 
    table_schema=[
        {'name': 'student_id', 'type': 'STRING'},
        {'name': 'date', 'type': 'STRING'},
        {'name': 'time', 'type': 'STRING'},
        {'name': 'level', 'type': 'INTEGER'}])
end = time.time()
print('\n\n loading time:', str(end - start))

# load activity table
start = time.time()
activity_table.to_gbq(
    'StudentLife.Activity',
    project_id,
    chunksize=10000, 
    if_exists='replace', 
    table_schema=[
        {'name': 'student_id', 'type': 'STRING'},
        {'name': 'date', 'type': 'STRING'},
        {'name': 'time', 'type': 'STRING'},
        {'name': 'relaxing', 'type': 'INTEGER'},
        {'name': 'working', 'type': 'INTEGER'},
        {'name': 'other_relaxing', 'type': 'INTEGER'},
        {'name': 'other_working', 'type': 'INTEGER'}])
end = time.time()
print('\n\n loading time:', str(end - start))

# load sensing activity table
start = time.time()
sen_act_table.to_gbq(
    'StudentLife.SenAct',
    project_id,
    chunksize=10000, 
    if_exists='replace', 
    table_schema=[
        {'name': 'student_id', 'type': 'STRING'},
        {'name': 'date', 'type': 'STRING'},
        {'name': 'activity_level', 'type': 'INTEGER'}])
end = time.time()
print('\n\n loading time:', str(end - start))


# Summary Statistics for Stress and Activity
# 1. Overall
# - For `Stress` table, count by `level`, because the numerical value 
#   is not monotonic-related , thus doing *avg/max/min/var* makes no sense.
# - For `Activity` table, we compute the *min/max/avg*.
# - For `SenAct` table, we also compute the *min/max/avg*.
# 
# 2. Individual

# count all the entries
total = client.query('''
    SELECT 
        COUNT(*) as total
    FROM `StudentLife.Stress`
''').to_dataframe().total[0]

# overall statistics of stress
overall_stress = client.query('''
    SELECT
        level, 
        COUNT(level) as count_stress_level
    FROM `StudentLife.Stress`
    GROUP BY level
''').to_dataframe()

# EMA_definition.json
stress_descr = [
    'A little stressed', 'Definitely stressed', 'Stressed out', \
    'Feeling good', 'Feeling great']

# append the description to the result table
overall_stress['description'] = stress_descr
overall_stress = overall_stress[['description', 'count_stress_level']]

print('Total count:', total)
print(overall_stress.to_string(index=False))

# overall statistics of activity
overall_activity = client.query('''
    SELECT 'total_count',
        COUNT(relaxing) as relaxing, 
        COUNT(working) as working, 
        COUNT(other_relaxing) as other_relaxing,
        COUNT(other_working) as other_working       
    FROM `StudentLife.Activity`
    UNION ALL
    SELECT 'min',
        MIN(relaxing), 
        MIN(working), 
        MIN(other_relaxing),
        MIN(other_working)      
    FROM `StudentLife.Activity`
    UNION ALL
    SELECT 'max',
        MAX(relaxing), 
        MAX(working), 
        MAX(other_relaxing),
        MAX(other_working)      
    FROM `StudentLife.Activity`
    UNION ALL
    SELECT 'avg',
        AVG(relaxing), 
        AVG(working), 
        AVG(other_relaxing),
        AVG(other_working)      
    FROM `StudentLife.Activity`
''').to_dataframe()

print(overall_activity.to_string(index=False))

# overall statistics of sensing activity
overall_sen_act = client.query('''
    SELECT 'total_count',
        COUNT(activity_level) as activity_level     
    FROM `StudentLife.SenAct`
    UNION ALL
    SELECT 'min', MIN(activity_level)     
    FROM `StudentLife.SenAct`
    UNION ALL
    SELECT 'max', MAX(activity_level)      
    FROM `StudentLife.SenAct`
    UNION ALL
    SELECT 'avg', AVG(activity_level)     
    FROM `StudentLife.SenAct`
''').to_dataframe()
print(overall_sen_act.to_string(index=False))

# individual statistics of stress
indi_stress = client.query('''
    SELECT 
        student_id,
        COUNT(student_id) as num_of_entries,
        COUNT(IF(level = 1, 1, NULL)) as a_little_stressed,
        COUNT(IF(level = 2, 1, NULL)) as definitely_stressed,
        COUNT(IF(level = 3, 1, NULL)) as stressed_out,
        COUNT(IF(level = 4, 1, NULL)) as feeling_good,
        COUNT(IF(level = 5, 1, NULL)) as feeling_great
    FROM `StudentLife.Stress`
    GROUP BY student_id
    ORDER BY student_id
''').to_dataframe()
print(indi_stress.to_string(index=False))

# individual statistics of activity
indi_activity = client.query('''
    SELECT 
        student_id,
        COUNT(student_id) as num_of_entries,
        AVG(relaxing) as relaxing, 
        AVG(working) as working, 
        AVG(other_relaxing) as other_relaxing,
        AVG(other_working) as other_working       
    FROM `StudentLife.Activity`
    GROUP BY student_id
    ORDER BY student_id
''').to_dataframe()
print('Average time allocation of activity for each individual')
print(indi_activity.to_string(index=False))

# individual statistics of sensing activity
indi_sen_act = client.query('''
    SELECT 
        student_id,
        COUNT(student_id) as num_of_entries,
        AVG(activity_level) as activity_level    
    FROM `StudentLife.SenAct`
    GROUP BY student_id
    ORDER BY student_id
''').to_dataframe()
print('Average sensing activity level for each individual')
print(indi_sen_act.to_string(index=False))


# Preprocess Stress Data
# Get daily averages. If a day (or more) is missing, impute the
# data from the last known day.
# 
# Since it makes no sense by "averaging" the level, we are supposed to 
# transform level into "stress_value", which better reflects the degree of stress.
# 
# mapping:
# 
# | level | description   | **stress_value**  |
# | ----: |--------------:| -----:|
# | 1     | A little stressed | 3 |
# | 2     | Definitely stressed |   4 |
# | 3     | Stressed out |    5 |
# | 4 | Feeling good | 2 |
# | 5 | Feeling great | 1 |

stress = stress_table[['student_id', 'date', 'level']]
stress['date']= pd.to_datetime(stress['date']) 

# mapping
def level_to_value(level):
    value = (level + 2) if level < 4 else (6 - level)
    return value

# append this new column called stress_value 
stress['stress_value'] =  [level_to_value(int(level)) \
    for level in stress.level]
stress.drop(columns=['level'], inplace=True)

# for each individual, group by date with the average of stress_value
stress = stress.groupby(
    by=['student_id', 'date'], 
    as_index=False).mean()
print(stress.head())

# Preprocess Activity Data
activity = activity_table[
    ['student_id', 'date', 'relaxing', 'working', \
     'other_relaxing', 'other_working']]
activity['date']= pd.to_datetime(activity['date']) 

# for each individual, group by date with the average of stress_value
activity = activity.groupby(
    by=['student_id', 'date'], 
    as_index=False).mean()
print(activity.head().to_string(index=False))
print(activity.shape)

# Preprocess Sensing Activity Data
sen_act = sen_act_table[
    ['student_id', 'date', 'activity_level']]
sen_act['date']= pd.to_datetime(sen_act['date']) 
print(sen_act.head().to_string(index=False))
print(sen_act.shape)

# Select the students with the most rows in df so that the data 
# quality in time series is reliable.

# take u16 and u10 as our time series
stress_u16 = stress.loc[stress['student_id'] == 'u16'].\
    drop('student_id', axis=1)
stress_u10 = stress.loc[stress['student_id'] == 'u10'].\
    drop('student_id', axis=1)

act_u16 = activity.loc[activity['student_id'] == 'u16'].\
    drop('student_id', axis=1)
act_u10 = activity.loc[activity['student_id'] == 'u10'].\
    drop('student_id', axis=1)

sen_act_u16 = sen_act.loc[sen_act['student_id'] == 'u16'].\
    drop('student_id', axis=1)
sen_act_u10 = sen_act.loc[sen_act['student_id'] == 'u10'].\
    drop('student_id', axis=1)

# process stress_u16
print(stress_u16.head(8).to_string(index=False))

# impute missing date and according stress_value from previous entry
stress_u16 = stress_u16.set_index('date').                resample('D').ffill().reset_index()
stress_u16_ = stress_u16.rename(
    columns={'date': 'ds', 'stress_value': 'y'}, 
    inplace=False)

print(stress_u16.head(8).to_string(index=False))
print(stress_u16.shape)

# process act_u16
# impute missing date and according activity from previous entry
act_u16 = act_u16.set_index('date').             resample('D').ffill().reset_index()

print(act_u16.head().to_string(index=False))
print(act_u16.shape)

# process sen_act_u16
print(sen_act_u16.shape)

# impute missing date and according activity from previous entry
sen_act_u16 = sen_act_u16.set_index('date').                resample('D').ffill().reset_index()

print(sen_act_u16.head().to_string(index=False))
print(sen_act_u16.shape)

# use Prophet to do time series analysis on dataset stress_u16
m = Prophet().fit(stress_u16_)
future = m.make_future_dataframe(periods=15)
forecast = m.predict(future)

# plot stress prediction with dataset
print('==== time series plot of stress_u16 ====')
fig1 = m.plot(forecast)
print(fig1)

# plot the component of "trend" and "weekly"
fig2 = m.plot_components(forecast)
print(fig2)

# use Prophet to do time series analysis on dataset act_u16
act_types = ['relaxing', 'working', 'other_relaxing', 'other_working']

for act in act_types:
    df = act_u16[['date', act]]
    df.rename(columns={'date': 'ds', act: 'y'}, 
              inplace=True)
    m = Prophet().fit(df)
    future = m.make_future_dataframe(periods=15)
    forecast = m.predict(future)

    # plot stress prediction with dataset
    fig1 = m.plot(forecast)
    print(fig1)

    # plot the component of "trend" and "weekly"
    fig2 = m.plot_components(forecast)
    print(fig2)

# use Prophet to do time series analysis on dataset sen_act_u16
sen_act_u16_ = sen_act_u16.rename(
    columns={'date': 'ds', 'activity_level': 'y'}, 
    inplace=False)
m = Prophet().fit(sen_act_u16_)
future = m.make_future_dataframe(periods=15)
forecast = m.predict(future)

# plot stress prediction with dataset
print('==== time series plot of sen_act_u16 ====')
fig1 = m.plot(forecast)
print(fig1)

# plot the component of "trend" and "weekly"
fig2 = m.plot_components(forecast)
print(fig2)

# impute data in stress_u10
stress_u10 = stress_u10.set_index('date').                resample('D').ffill().reset_index()
stress_u10_ = stress_u10.rename(
    columns={'date': 'ds', 'stress_value': 'y'}, 
    inplace=False)

# use Prophet to do time series analysis on dataset stress_u10
m = Prophet().fit(stress_u10_)
future = m.make_future_dataframe(periods=15)
forecast = m.predict(future)

# plot stress prediction with dataset
print('==== time series plot of stress_u10 ====')
fig1 = m.plot(forecast)
print(fig1)

# plot the component of "trend" and "weekly"
fig2 = m.plot_components(forecast)
print(fig2)

# process act_u10
# impute missing date and according activity from previous entry
act_u10 = act_u10.set_index('date').             resample('D').ffill().reset_index()

print(act_u10.head().to_string(index=False))
print(act_u10.shape)

# impute data in sen_act_u10
sen_act_u10 = sen_act_u10.set_index('date').                resample('D').ffill().reset_index()
sen_act_u10_ = sen_act_u10.rename(
    columns={'date': 'ds', 'activity_level': 'y'}, 
    inplace=False)

# use Prophet to do time series analysis on dataset act_u10
act_types = ['relaxing', 'working', 'other_relaxing', 'other_working']

for act in act_types:
    df = act_u10[['date', act]]
    df.rename(columns={'date': 'ds', act: 'y'}, 
              inplace=True)
    m = Prophet().fit(df)
    future = m.make_future_dataframe(periods=15)
    forecast = m.predict(future)

    # plot stress prediction with dataset
    fig1 = m.plot(forecast)
    print(fig1)

    # plot the component of "trend" and "weekly"
    fig2 = m.plot_components(forecast)
    print(fig2)

# use Prophet to do time series analysis on sen_act_u10
m = Prophet().fit(sen_act_u10_)
future = m.make_future_dataframe(periods=15)
forecast = m.predict(future)

# plot stress prediction with dataset
print('==== time series plot of sen_act_u10 ====')
fig1 = m.plot(forecast)
print(fig1)

# plot the component of "trend" and "weekly"
fig2 = m.plot_components(forecast)
print(fig2)


# Compute the Correlation of Stress and Activity
# - Overall: daily
# - Component: weekly


# Correlation: EMA Activity ~ Stress
# overall level of correlation for u16
df_u16 = pd.merge(stress_u16, act_u16)
# calculate the corr without date for u16
print(df_u16.iloc[:, 1:].corr(method='pearson'))

# weekly level of correlation for u16
df_u16['week'] = df_u16['date'].dt.week
df_u16_weekly = df_u16[
    ['week', 'stress_value', 'relaxing', 'working', \
     'other_relaxing', 'other_working']]

# take weekly means
df_u16_weekly =     df_u16_weekly.groupby(by=['week'], as_index=False).mean()
print(df_u16_weekly.iloc[:, 1:].corr(method='pearson'))

# overall level of correlation for u10
df_u10 = pd.merge(stress_u10, act_u10)

# calculate the corr without date for u10
print(df_u10.iloc[:,1:].corr(method='pearson'))

# weekly level of correlation for u10
df_u10['week'] = df_u10['date'].dt.week
df_u10_weekly = df_u10[
    ['week', 'stress_value', 'relaxing', 'working', \
     'other_relaxing', 'other_working']]

# take weekly means
df_u10_weekly =     df_u10_weekly.groupby(by=['week'], as_index=False).mean()
print(df_u10_weekly.iloc[:,1:].corr(method='pearson'))


# Correlation: Sensing Activity ~ Stress
# overall level of correlation for u16
df_u16 = pd.merge(stress_u16, sen_act_u16)

# calculate the corr without date for u16
print(df_u16.iloc[:, 1:].corr(method='pearson'))

# weekly level of correlation for u16
df_u16['week'] = df_u16['date'].dt.week
df_u16_weekly = df_u16[
    ['week', 'stress_value', 'activity_level']]

# take weekly means
df_u16_weekly =     df_u16_weekly.groupby(by=['week'], as_index=False).mean()
print(df_u16_weekly.iloc[:, 1:].corr(method='pearson'))

# overall level of correlation for u10
df_u10 = pd.merge(stress_u10, sen_act_u10)

# calculate the corr without date for u10
print(df_u10.iloc[:, 1:].corr(method='pearson'))

# weekly level of correlation for u10
df_u10['week'] = df_u10['date'].dt.week
df_u10_weekly = df_u10[
    ['week', 'stress_value', 'activity_level']]

# take weekly means
df_u10_weekly =     df_u10_weekly.groupby(by=['week'], as_index=False).mean()
print(df_u10_weekly.iloc[:, 1:].corr(method='pearson'))


# Task 2: Correlation of More Attributes
# Load Sleep Data
sleep_df = [] # a list storing the df of each qualified .json file
json_sleep_list = get_json_list('Sleep')

for file_name in json_sleep_list:
    # read json
    df = pd.read_json(
        os.path.join(stu_path, 'EMA/response/Sleep', file_name))

    # time format transformation
    df.columns = df.columns.str.replace(' ', '_')
    df['resp_time'] = pd.to_datetime(df['resp_time'], unit='s')
    df['date'] = df['resp_time'].dt.date
    df['time'] = df['resp_time'].dt.time
        
    # take only the columns we need
    columns_required = ['date', 'hour']
    df = df[columns_required]

    # add student_id from filename
    student_id = file_name[6: 9]
    df.insert(0, 'student_id', student_id)

    # in each accepted file, drop the rows with NaN
    df.dropna(subset = columns_required, inplace=True)

    sleep_df.append(df)

# concatenate all the .json files into one big table
sleep_table = pd.concat(sleep_df)
sleep_table.sort_values(
    by=['student_id', 'date'],
    inplace=True)

# get daily average
sleep_table = sleep_table.groupby(
    by=['student_id', 'date'], 
    as_index=False).mean()
print(sleep_table.head().to_string(index=False))

# Get mean
sleep_table.drop(['date'], axis=1)
sleep_table = sleep_table.groupby('student_id').mean()
sleep_table = sleep_table.reset_index()
print(sleep_table.head())

# Load Conversation Data
conv_df = []
csv_conv_list = get_csv_list('conversation')

for file_name in csv_conv_list:
    df = pd.read_csv(
        os.path.join(stu_path, 'sensing/conversation', file_name))

    # reformat column name
    df.columns = df.columns.str.replace(' ', '_')
    df.rename(columns={'_end_timestamp': 'end_timestamp'}, 
              inplace=True)

    # add student_id column from file name
    student_id = file_name[13: 16]
    df.insert(0, 'student_id', student_id)

    # convert timestamp
    df['start_timestamp'] = pd.to_datetime(
        df['start_timestamp'], unit='s')
    df['end_timestamp'] = pd.to_datetime(
        df['end_timestamp'], unit='s')
    
    # add a conv_duration column to store the difference
    # between the two timestamps in a row
    df['conv_duration'] = (df['end_timestamp'] - df['start_timestamp'])        .dt.seconds.values

    # add date column for further grouping
    df['date'] = df['start_timestamp'].dt.date
     
    conv_df.append(df)

conv_table = pd.concat(conv_df)
print(conv_table.head().to_string(index=False))

# compute the conv_duration_day/evening
conv_table['conv_duration_day'] = np.where(
    ((conv_table['start_timestamp'].dt.hour >= 9) \
     & (conv_table['start_timestamp'].dt.hour < 18)), 
    conv_table['conv_duration'], 
    0)
conv_table['conv_duration_evening'] =     conv_table['conv_duration'] - conv_table['conv_duration_day']

# count frequency
conv_table['conv_freq'] = 1
conv_table['conv_freq_day'] = np.where(
    ((conv_table['start_timestamp'].dt.hour >= 9) \
     & (conv_table['start_timestamp'].dt.hour < 18)), 
    1, 
    0)
conv_table['conv_freq_evening'] = 1 - conv_table['conv_freq_day']

# take the columns we nead
conv_table = conv_table[
    ['student_id', 'date', 'conv_duration', 'conv_duration_day', \
     'conv_duration_evening', 'conv_freq', 'conv_freq_day', \
     'conv_freq_evening']]
print(conv_table.head().to_string(index=False))

# group by student_id and date
agg_function = {
    'conv_duration': 'mean', 
    'conv_duration_day': 'mean', 
    'conv_duration_evening': 'mean', 
    'conv_freq': 'sum',
    'conv_freq_day': 'sum',
    'conv_freq_evening': 'sum'}
conv_table = conv_table.groupby(
    by=['student_id', 'date'],
    as_index=False).agg(agg_function)
print(conv_table.head().to_string(index=False))

# Get mean
conv_table.drop(['date'], axis=1)
conv_table = conv_table.groupby('student_id').mean()
print(conv_table.head().to_string(index=False))

# Load PerceivedStressScale Data
pstress = pd.read_csv(
    os.path.join(stu_path, 'survey/PerceivedStressScale.csv'))
print(pstress.head().to_string(index=False))

# select columns
cols = pstress.columns
pstress = pstress[[cols[0], cols[1], cols[4]]]

# rename columns
pstress.columns = ["student_id" ,"type", "stressed"]

# re-encode values
pstress = pstress.replace(
    to_replace =['Very often', 'Fairly often', 'Sometime', \
                 'Almost never', 'Never'], 
    value = [4,3,2,1,0]) 
pstress.stressed = pd.to_numeric(pstress.stressed)
print(pstress.head().to_string(index=False))

# Correlation Analysis
# join all the attributed together
merged = pd.merge(conv_table, pstress, on='student_id')
sleep_merged = sleep_table[['student_id', 'hour']]
merged = pd.merge(merged, sleep_merged, on='student_id')
print(merged.head().to_string(index=False))

# pre and post design
merged_post = merged.loc[merged["type"] == "post"]
merged_pre = merged.loc[merged["type"] == "pre"]

# Correlation with post stress
post_corr = pg.pairwise_corr(
    data=merged_post,
    columns=[
        ['stressed'], 
        ['conv_duration', 'conv_duration_day', \
         'conv_duration_evening', 'conv_freq', \
         'conv_freq_day', 'conv_freq_evening', 'hour']], 
    method='pearson')

print('post correlation\n',
    post_corr.sort_values(by=['p-unc'])[['X', 'Y', 'r', 'p-unc']].head())

pre_corr = pg.pairwise_corr(
    data=merged_pre,
    columns=[
        ['stressed'], 
        ['conv_duration', 'conv_duration_day', \
         'conv_duration_evening', 'conv_freq', \
         'conv_freq_day', 'conv_freq_evening', 'hour']], 
    method='pearson')
print('pre correlation\n',
    post_corr.sort_values(by=['p-unc'])[['X', 'Y', 'r', 'p-unc']].head(8))

# %% [markdown]
# ### Classification

# %%
stress_df = []
json_stress_list = get_json_list('Stress')

# for file_name in json_files:
for file_name in json_stress_list:
    # read json
    df = pd.read_json(
        os.path.join(stu_path, 'EMA/response/Stress', file_name),
        dtype={'level': int})

    # only accept .json files containing the two columns we require   
    if 'resp_time' in df.columns.to_list()         and 'level' in df.columns.to_list(): 
        # accept this file
        print(file_name, df.columns.to_list(), '=' * 5 + '>', 'accepted')

        # reformat column names
        df.columns = df.columns.str.replace(' ', '_')
        
        # reformat timestamp
        df['resp_time'] = pd.to_datetime(df['resp_time'], unit='s')
        df['date'] = df['resp_time'].dt.date
        df['time'] = df['resp_time'].dt.time

        # add student_id from filename
        student_id = file_name[7: 10]
        df.insert(0, 'student_id', student_id)

        # drop the rows with NaN in 'level' or 'resp_time'
        df.dropna(subset = ['resp_time', 'level'], inplace=True)
        
        # take only the columns we need
        df = df[['student_id', 'date', 'time', 'level']]
        stress_df.append(df)
    
    else: # reject this file
        print(file_name, df.columns.to_list(), '=' * 5 + '>', 'rejected')

# concatenate all the .json files into one big table
stress_table = pd.concat(stress_df)
print(stress_table.head().to_string(index=False))

stress_table = stress_table[['student_id', 'level']]
stress_table.level = stress_table['level'].astype('int')
print(stress_table.head())

# group by median
stress_table = stress_table.groupby('student_id').apply(np.median)
stress_table = stress_table.reset_index()
stress_table.columns = ['student_id', 'level']
stress_table.level = stress_table['level'].astype('int')
print(stress_table.head())

# Get mean of activity_table
activity_table = activity_table.groupby('student_id').mean()
activity_table = activity_table.reset_index()
print(activity_table.head())

conv_df = []
csv_conv_list = get_csv_list('conversation')

for file_name in csv_conv_list:
    df = pd.read_csv(
        os.path.join(stu_path, 'sensing/conversation', file_name))

    # reformat column name
    df.columns = df.columns.str.replace(' ', '_')
    df.rename(columns={'_end_timestamp': 'end_timestamp'}, 
              inplace=True)

    # add student_id column from file name
    student_id = file_name[13: 16]
    df.insert(0, 'student_id', student_id)

    # convert timestamp
    df['start_timestamp'] = pd.to_datetime(
        df['start_timestamp'], unit='s')
    df['end_timestamp'] = pd.to_datetime(
        df['end_timestamp'], unit='s')
    
    # add a conv_duration column to store the difference
    # between the two timestamps in a row
    df['conv_duration'] = (df['end_timestamp'] - df['start_timestamp'])        .dt.seconds.values

    # add date column for further grouping
    df['date'] = df['start_timestamp'].dt.date
     
    conv_df.append(df)

conv_table = pd.concat(conv_df)
print(conv_table.head().to_string(index=False))

conv_table = conv_table.groupby('student_id').mean()
print(conv_table.head())

# join all the tables
df = pd.merge(activity_table, conv_table, on =["student_id"])
df = pd.merge(df, sleep_table, on = ["student_id"])
df = pd.merge(df, stress_table, on = ["student_id"])
print(df.head())

df = df.round(3) 
print(df.info())

# plot the histogram of the distribution of our target stress level
df.hist(column='level')

import matplotlib.pyplot as plt
# merge the target class into 2 classes
# regard 1 and 4 as healthy(0), 2 and 3 unhealthy(1)

df['target'] = df['level'].apply(
    lambda x: 'healthy' if x in [1, 4] else 'unhealthy')
df['target'].value_counts().plot.bar()


from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics

X = df[['hour', 'working', 'other_working', 'relaxing',       'other_relaxing', 'conv_duration', 'hour']]  
y = df['target']  # Labels

# Split dataset into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

# create a Gaussian Classifier
clf_1 = RandomForestClassifier().fit(X_train,y_train)
y_pred = clf_1.predict(X_test)

# Model Accuracy, how often is the classifier correct?
print("Accuracy of RandomForest:",metrics.accuracy_score(y_test, y_pred))

from sklearn import svm

# create a svm Classifier
clf_2 = svm.SVC(kernel='linear') # Linear Kernel
clf_2.fit(X_train, y_train)

# predict the response for test dataset
y_pred = clf_2.predict(X_test)
print('Accuracy:',metrics.accuracy_score(y_test, y_pred))