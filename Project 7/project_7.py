import pandas as pd
import numpy as np

import linearmodels
from linearmodels.panel import PanelOLS, RandomEffects, compare
import statsmodels.api as sm

!wget https://raw.githubusercontent.com/sglyon/econtools/master/Python/econtools/metrics.py -O metrics.py
from metrics import hausman

pd.set_option('display.max_columns', 6)
pd.options.display.float_format = '{:,.2f}'.format

# load data
url = 'https://git.imp.fu-berlin.de/yuxuac94/storage/'\
    'raw/master/IPADS-2019/week7/grunfeld.csv'
data = pd.read_csv(url)
print(data.isnull().sum())

# preprocessing
data.rename(
    columns={'C ': 'C', 'YEAR': 'year', 'FIRM': 'firm'}, 
    inplace=True)
data.year = data.year.astype(np.int64)
print(data.describe())

data = data.set_index(['firm', 'year'])
print(data.head())

# fixed effects
exog_vars = ['F','C']
exog = sm.add_constant(data[exog_vars])

fe_model = PanelOLS(data.I, exog, entity_effects=True)
fe_res = fe_model.fit()
print(fe_res)

# random effects
re_model = RandomEffects(data.I, exog)
re_res = re_model.fit()
print(re_res)

print(compare({'FE':fe_res,'RE':re_res}))
print(hausman(fe_res, re_res))