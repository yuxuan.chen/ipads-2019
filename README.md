This is the repository of Yuxuan Chen's submissions of Assigment for the 2019/20 WiSo course "Introduction to Profile Areas in Data Science";

## Table of Content

#### [Assignment 1](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%201)  deadline: 31.10.2019
- [project_1.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%201/project_1.py) 
  -- code to be submitted for the project to build a NN to predict heart disease
- [report_1.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%201/report_1.pdf) 
  -- report to be submitted on the project
- [project_1.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%201/project_1.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment 2](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%202)  deadline: 31.10.2019
- [project_2.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%202/project_2.py) 
  -- code to be submitted for the project to compare feature selection/extraction on RF/SVM 
- [report_2.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%202/report_2.pdf) 
  -- report to be submitted on the project
- [project_2.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%202/project_2.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment 3](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%203)  deadline: 7.11.2019
- [project_3.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%203/project_3.py) 
  -- code to be submitted for the project to predict stroke with two classifiers on spark
- [report_3.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%203/report_3.pdf) 
  -- report to be submitted on the project
- [project_3.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%203/project_3.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment 4](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%204)  deadline: 14.11.2019
- [project_4.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%204/project_4.py) 
  -- code to be submitted for the project to conduct queries with BigQuery on genomes dataset
- [report_4.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%204/report_4.pdf) 
  -- report to be submitted on the project
- [project_4.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%204/project_4.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### Assignment 5  deadline: 21.11.2019
- [report_1_latex.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%201/report_1_latex.pdf) 
  -- in the directory [Project 1](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%201) along with earlier submissions
- [report_2_latex.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%202/report_2_latex.pdf)
  -- in the directory [Project 2](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%202) along with earlier submissions
- [report_3_latex.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%203/report_3_latex.pdf)
  -- in the directory [Project 3](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%203) along with earlier submissions
- [report_4_latex.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%204/report_4_latex.pdf) 
  -- in the directory [Project 4](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%204) along with earlier submissions

#### [Assignment 6](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%206)  deadline: 28.11.2019
- [project_6.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%206/project_6.py) 
  -- code to be submitted for the project to do time series analysis on biom data
- [report_6.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%206/report_6.pdf) 
  -- report to be submitted on the project
- [project_6.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%206/project_6.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment 7](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%207)  deadline: 5.12.2019
- [project_7.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%207/project_7.py) 
  -- code to be submitted for the project to do panel analysis on datasets in social science
- [report_7.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%207/report_7.pdf) 
  -- report to be submitted on the project
- [project_7.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%207/project_7.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment 8](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%208)  deadline: 12.12.2019
- [project_8.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%208/project_8.py) 
  -- code to be submitted for the project to do correlation and classification on StudentLife dataset.
- [report_8.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%208/report_8.pdf) 
  -- report to be submitted on the project
- [project_8.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%208/project_8.ipynb)
  -- IPython Notebook version for the sumbitted Python file

#### [Assignment 9](https://gitlab.com/yuxuan.chen/ipads-2019/-/tree/master/Project%209)  deadline: 12.19.2019
- [project_9.py](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%209/project_9.py) 
  -- code to be submitted for the project to do SVD and LDA analysis on real-world data.
- [report_9.pdf](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%209/report_9.pdf) 
  -- report to be submitted on the project
- [project_9.ipynb](https://gitlab.com/yuxuan.chen/ipads-2019/-/blob/master/Project%209/project_9.ipynb)
  -- IPython Notebook version for the sumbitted Python file


## Dependencies
- **Python 3.5+**
- **PyTorch 0.7+**
