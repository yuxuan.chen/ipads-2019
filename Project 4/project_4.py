from IPython import get_ipython
from google.colab import auth
auth.authenticate_user()
print('Authenticated')

get_ipython().run_line_magic('load_ext', 'google.colab.data_table')
project_id = 'tutorial-258411'
get_ipython().run_line_magic('env', 'GCLOUD_PROJECT=tutorial-258411')

import pandas as pd
!wget https://raw.githubusercontent.com/SohierDane/BigQuery_Helper/master/bq_helper.py
from bq_helper import BigQueryHelper

bq_assistant = BigQueryHelper(
    'bigquery-public-data', 'human_genome_variants')
bq_assistant.list_tables()

# count the number of rows
QUERY = """
        #standardSQL
        SELECT
          COUNT(1) AS number_of_rows
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823`
        """
bq_assistant.estimate_query_size(QUERY)

df = bq_assistant.query_to_pandas_safe(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

# sum up the length of call arrays
QUERY = """
        #standardSQL
        SELECT
          SUM(ARRAY_LENGTH(call)) AS number_of_calls
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823`
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas_safe(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          COUNT(1) AS number_of_real_variants
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v, v.call call
        WHERE
            EXISTS (SELECT 1
                FROM UNNEST(v.alternate_bases) AS alt
            WHERE
                alt.alt NOT IN ("<NON_REF>", "<*>"))
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas_safe(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        #standardSQL
        SELECT
          call.name AS call_name,
          COUNT(call.name) AS call_count_for_call_set
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v, v.call
        GROUP BY
          call_name
        ORDER BY
          call_name
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          call.name AS call_name,
          COUNT(call.name) AS call_count_for_call_set
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v, v.call
        WHERE
          EXISTS (SELECT 1
                    FROM UNNEST(v.alternate_bases) AS alt
                  WHERE
                    alt.alt NOT IN ("<NON_REF>", "<*>"))
        GROUP BY
          call_name
        ORDER BY
          call_name
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          COUNT(DISTINCT call.name) AS number_of_callsets
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v,  v.call
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          reference_name,
          COUNT(reference_name) AS number_of_variant_rows
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v
        WHERE
          EXISTS (SELECT 1
                    FROM UNNEST(v.call) AS call, UNNEST(call.genotype) AS gt
                  WHERE gt > 0)
        GROUP BY
          reference_name
        ORDER BY
          CASE
            WHEN SAFE_CAST(REGEXP_REPLACE(reference_name, '^chr', '') AS INT64) < 10
                THEN CONCAT('0', REGEXP_REPLACE(reference_name, '^chr', ''))
                ELSE REGEXP_REPLACE(reference_name, '^chr', '')
          END
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          call_filter,
          COUNT(call_filter) AS number_of_calls
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v,
          v.call,
          UNNEST(call.FILTER) AS call_filter
        GROUP BY
          call_filter
        ORDER BY
          number_of_calls
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas_safe(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          call.name AS call_name,
          COUNT(1) AS number_of_calls
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v, v.call
        WHERE
          NOT EXISTS (SELECT 1 FROM UNNEST(call.FILTER) AS call_filter WHERE call_filter != 'PASS')
        GROUP BY
          call_name
        ORDER BY
          call_name
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          call.name AS call_name,
          COUNT(1) AS number_of_calls
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v, v.call
        WHERE
          NOT EXISTS (SELECT 1 FROM UNNEST(call.FILTER) AS call_filter WHERE call_filter != 'PASS')
          AND EXISTS (SELECT 1 FROM UNNEST(call.genotype) as gt WHERE gt > 0)
        GROUP BY
          call_name
        ORDER BY
          call_name
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)

QUERY = """
        SELECT
          reference_name,
        COUNT(reference_name) AS number_of_variant_rows
        FROM
          `bigquery-public-data.human_genome_variants.platinum_genomes_deepvariant_variants_20180823` v
        WHERE
          EXISTS (SELECT 1
                    FROM UNNEST(v.call) AS call
                  WHERE EXISTS (SELECT 1
                                  FROM UNNEST(call.genotype) AS gt
                                WHERE gt > 0))
        GROUP BY
          reference_name
        ORDER BY
          reference_name
        """
print(bq_assistant.estimate_query_size(QUERY))

df = bq_assistant.query_to_pandas(QUERY)
print('Size of dataframe: {} Bytes'.format(
    int(df.memory_usage(index=True, deep=True).sum())))
print(df)