from google.colab import drive
drive.mount('/content/drive')

import numpy as np
import pandas as pd
import os
from scipy.sparse import dok_matrix
from scipy.sparse.linalg import svds
import seaborn as sns

# Hands-On 1: Sample Big Dataset 

# load the three files
dir_path = '/content/drive/My Drive/Colab Notebooks/data/sample_dataset'
users = pd.read_csv(
    os.path.join(dir_path, 'users.csv'))
likes = pd.read_csv(
    os.path.join(dir_path, 'likes.csv'))
ul = pd.read_csv(
    os.path.join(dir_path, 'users-likes.csv'))

# check the dimension, head and tail of the dataframe users
print('dim of dataframe users: \n', users.shape)
print('head of dataframe users: \n', users.head())
print('tail of dataframe users: \n', users.tail())

# check the dimension, head and tail of the dataframe users
print('dim of dataframe likes: \n', likes.shape)
print('head of dataframe likes: \n', likes.head())
print('tail of dataframe likes: \n', likes.tail())

# check the dimension, head and tail of the dataframe ul
print('dim of dataframe ul: \n', ul.shape)
print('head of dataframe ul: \n', ul.head())
print('tail of dataframe ul: \n', ul.tail())

# Hands-On 2: Constructing a User-Like Matrix

# count the likes for each user
likes_per_user = ul.groupby(
    by='userid', 
    as_index=False
).count().rename(columns={'likeid': 'count'})[['userid', 'count']]

print(likes_per_user.head().to_string(index=False))
print('\nstatistics summary of likes per user: \n',       likes_per_user['count'].describe())

# count the users for each like
users_per_like = ul.groupby(
    by='likeid', 
    as_index=False
).count().rename(columns={'userid': 'count'})[['likeid', 'count']]

print(users_per_like.head().to_string(index=False))
print('\nstatistics summary of users per like: \n',       users_per_like['count'].describe())

# previous sum of dimensions
sum_dim_pre = users_per_like.shape[0] + likes_per_user.shape[0]

# current sum of dimensions
sum_dim_cur = 0

# repeat until the count becomes stably constant
while(sum_dim_cur != sum_dim_pre):
    # update previous dim
    sum_dim_pre = sum_dim_cur
    
    # trim the userid with count less than 50
    likes_per_user = likes_per_user[
        likes_per_user['count'] >= 50]

    # trim the like with count less than 150
    users_per_like = users_per_like[
        users_per_like['count'] >= 150]
    
    # filter the entries in ul
    ul = ul[
        ul['userid'].isin(likes_per_user['userid']
        ) & ul['likeid'].isin(users_per_like['likeid'])]
    
    # count again
    likes_per_user = ul.groupby(
        by='userid', 
        as_index=False
    ).count().rename(columns={'likeid': 'count'})[['userid', 'count']]
    users_per_like = ul.groupby(
        by='likeid', 
        as_index=False
    ).count().rename(columns={'userid': 'count'})[['likeid', 'count']]
    
    # compute the current dimension
    sum_dim_cur = users_per_like.shape[0] + likes_per_user.shape[0]

# check user (trimmed)
print(likes_per_user.head().to_string(index=False))
print('\nstatistics summary of trimmed likes per user: \n', 
    likes_per_user['count'].describe())

# check like (trimmed)
print(users_per_like.head().to_string(index=False))
print('\nstatistics summary of trimmed users per like: \n', 
    users_per_like['count'].describe())

# re-index ul and check
ul.reset_index(drop=True, inplace=True)
print(ul)

# update the tables of users and likes
users = users[users['userid'].isin(likes_per_user['userid'])]
likes = likes[likes['likeid'].isin(users_per_like['likeid'])]

# re-index the two tables
users.reset_index(drop=True, inplace=True)
likes.reset_index(drop=True, inplace=True)

print(users.head())
print(likes.head())


# build a sparse matrix of the trimmed data

# construct the sparse matrix from matching:
#     (1) ul.userid -- users.userid ===> user_row
#     (2) ul.likeid -- likes.likeid ===> like_row
ul['user_row']= pd.merge(ul, users.reset_index(), 'left')['index']
ul['like_row']= pd.merge(ul, likes.reset_index(), 'left')['index']
print(ul)

# build sparse matrix
M = dok_matrix((users.shape[0], likes.shape[0]), dtype=np.float32)
for i, j in zip(ul['user_row'], ul['like_row']):
    M[i, j] = 1

# do SVD of k=5
u, s, vt = svds(M, k=5)

# svd for user with k=5
u_svd = pd.DataFrame(
    {'SVD1': u[:, 0], 
     'SVD2': u[:, 1], 
     'SVD3': u[:, 2], 
     'SVD4': u[:, 3], 
     'SVD5': u[:, 4]})
u_svd.index = users['userid']
print(u_svd)

# svd for like with k=3
v = np.transpose(vt)
v_svd = pd.DataFrame(
    {'SVD1': v[:, 0], 
     'SVD2': v[:, 1], 
     'SVD3': v[:, 2], 
     'SVD4': v[:, 3], 
     'SVD5': v[:, 4]})
v_svd.index = likes['name']
print(v_svd)


from sklearn.decomposition import LatentDirichletAllocation as LDA

# k is n_components
# alpha=10 is doc_topic_prior
# delta=0.1 is topic_word_prior
lda = LDA(n_components=5, 
          doc_topic_prior=10,  
          topic_word_prior=0.1, 
          random_state=68)
lda.fit(M)

beta_ = np.transpose(lda.components_)
# normalize by column
beta = beta_/beta_.sum(axis=0,keepdims=1)
beta_lda = pd.DataFrame(
    {'LDA1': beta[:, 0], 
     'LDA2': beta[:, 1], 
     'LDA3': beta[:, 2], 
     'LDA4': beta[:, 3], 
     'LDA5': beta[:, 4]})
beta_lda.index = likes['name']
print(beta_lda)

# plot log-likelihood for different k of LDA
log_results = []
for i in range(2, 6):
    lda = LDA(n_components=i, 
              doc_topic_prior=10, 
              topic_word_prior=0.1, 
              random_state=68)
    lda.fit(M)
    log_results.append(lda.bound_)

df_logs = pd.DataFrame(
    {'k': range(2, 6), 
     'log': log_results})
ax = df_logs.plot.bar(x='k', y='log', rot=0)
# from the plot we can see that k=4 is optimal

# get the top likes for each of the 5 LDAs
for i in range(1, 6):
    # sort by value in an LDA-column
    lda_colname = 'LDA' + str(i)
    df = beta_lda.sort_values(by=[lda_colname], ascending=False)
    top_10_likes = df.index[:10]

    # print the results
    print(lda_colname, top_10_likes)

# merge the users and the LDAs
svd_merged = pd.merge(left=users, 
                      right=u_svd, 
                      how='left', 
                      left_on='userid', 
                      right_index=True)

# compute corr
corr = pd.DataFrame()
for corr_row in users.columns[1:]:
    for corr_col in u_svd.columns:
        corr.loc[corr_row, corr_col] =             svd_merged.corr().loc[corr_row, corr_col]

# plot corr
sns.heatmap(corr)

# get the top likes for each of the 5 SVDs
for i in range(1, 6):
    # sort by value in an LDA-column
    svd_colname = 'SVD' + str(i)
    df = v_svd.sort_values(by=[svd_colname], ascending=False)
    top_10_likes = df.index[:10]

    # print the results
    print(svd_colname, top_10_likes)