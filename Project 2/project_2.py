import pandas as pd
import numpy as np
import seaborn
import matplotlib.pyplot as plt
from scipy import stats

import sklearn
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score,confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

import warnings
warnings.filterwarnings('ignore')

# load data
data_url = 'https://gitlab.com/yuxuan.chen/storage/'\
    '-/raw/master/IPADS-2019/week2/data.csv'
data = pd.read_csv(data_url)

# remove useless columns
data.drop(['Unnamed: 32', 'id'], axis=1, inplace=True)
print(data.describe())

# re-encode
data['diagnosis'].replace('M', 1, inplace=True)
data['diagnosis'].replace('B', 0, inplace=True)
data_mal = data[data['diagnosis'] == 1]
data_ben = data[data['diagnosis'] == 0]
x = data.drop(['diagnosis'], axis=1)
y = data['diagnosis']

# split data train 80% and test 20%
x_train, x_test, y_train, y_test = \
    train_test_split(x, y, test_size=0.2, random_state=42)

# box-plot to observe outliers
melted_data = pd.melt(frame=data, 
                      id_vars='diagnosis', 
                      value_vars=['radius_mean', 'texture_mean'])
plt.figure(figsize = (6, 4))
seaborn.boxplot(x='variable', 
                y='value', 
                hue='diagnosis', 
                data=melted_data)
plt.show()

# effect size
mean_diff = data_mal.radius_mean.mean() - data_ben.radius_mean.mean()
var_ben = data_ben.radius_mean.var()
var_mal = data_mal.radius_mean.var()
var_pooled = (len(data_ben) * var_ben + len(data_mal) * var_mal) 
    / float(len(data_ben) + len(data_mal))
effect_size = mean_diff / np.sqrt(var_pooled)
print('Effect size:', effect_size)

# correlation heatmap and numerics
f,ax=plt.subplots(figsize = (18,15))
seaborn.heatmap(data=data.corr(), 
                annot=True, 
                linewidths=0.5, 
                fmt = '.1f', 
                ax=ax)
plt.xticks(rotation=90)
plt.yticks(rotation=0)
plt.title('Correlation Heatmap')
plt.show()
corr = data.corr()['diagnosis'].abs().sort_values()
print(corr)

# feature selection(fs)
drop_list = ['perimeter_mean', 'radius_mean', 'compactness_mean', 
             'concave points_mean', 'radius_se', 'perimeter_se', 
             'radius_worst', 'perimeter_worst', 'compactness_worst', 
             'concave points_worst', 'compactness_se', 
             'concave points_se', 'texture_worst', 'area_worst']
x_train_fs = x_train.drop(drop_list,axis = 1)
x_test_fs = x_test.drop(drop_list,axis = 1)


# %%
from sklearn.feature_selection import RFECV

# The "accuracy" scoring is prop. to the number of correct classifications
clf_rf = RandomForestClassifier()

# 5-fold cross-validation
rfecv = RFECV(estimator=clf_rf, step=1, cv=5,scoring='accuracy')
rfecv = rfecv.fit(x_train_fs, y_train)

print('Optimal number of features :', rfecv.n_features_)
print('Best features :', x_train_fs.columns[rfecv.support_])

# Plot number of features VS. cross-validation scores
plt.figure()
plt.xlabel("Number of features selected")
plt.ylabel("Cross validation score of number of selected features")
plt.plot(range(1, len(rfecv.grid_scores_) + 1), rfecv.grid_scores_)
plt.show()

# further feature selection(fs)
drop_list = ['fractal_dimension_mean', 'texture_se', 'smoothness_se']
x_train_fs = x_train.drop(drop_list,axis = 1)
x_test_fs = x_test.drop(drop_list,axis = 1)

# RF for feature selection
classifier = RandomForestClassifier(n_estimators=10, 
                                    criterion='entropy', 
                                    random_state=0)      
classifier = classifier.fit(x_train_fs, y_train)
accuracy = accuracy_score(y_test, classifier.predict(x_test_fs))
print('Accuracy of RF for feature selection is:', accuracy)

# SVM for feature selection
classifier = sklearn.svm.SVC(kernel='linear', 
                             C=0.1, 
                             gamma=10, 
                             probability=True)
classifier.fit(x_train_fs, y_train)
prediction = classifier.predict(x_test_fs)
accuracy = accuracy_score(prediction, y_test)
print('Accuracy of SVM for feature selection is:', accuracy)

# PCA analysis
scaler = StandardScaler()
x_train_fe = scaler.fit_transform(x_train)
x_test_fe = scaler.fit_transform(x_test)
pca = PCA()
pca.fit(x_train_fe)

plt.figure(1, figsize=(14, 13))
plt.clf()
plt.axes([.2, .2, .7, .7])
plt.plot(pca.explained_variance_ratio_, linewidth=2)
plt.axis('tight')
plt.xlabel('n_components')
plt.ylabel('explained_variance_ratio_')
print(pca.explained_variance_ratio_)

# select optimal k for PCA
rf_results, svm_results = [], []
for k in range(1, 16):
    pca = PCA(n_components=k)
    x_train_fe_ = pca.fit_transform(x_train_fe)
    x_test_fe_ = pca.transform(x_test_fe)
    
    # RF + PCA
    classifier = RandomForestClassifier(n_estimators=10, 
                                        criterion='entropy', 
                                        random_state=0)
    classifier.fit(x_train_fe_, y_train)
    y_pred = classifier.predict(x_test_fe_)
    accuracy = accuracy_score(y_test, y_pred)
    rf_results.append(accuracy)
    print('k=', k, ', Accuracy of RF is:', accuracy)
    
    # SVM + PCA
    classifier = sklearn.svm.SVC(kernel='linear', 
                                C=0.1, 
                                gamma=10, 
                                probability=True)
    classifier.fit(x_train_fe_, y_train)
    prediction = classifier.predict(x_test_fe_)
    accuracy = accuracy_score(prediction, y_test)
    svm_results.append(accuracy)
    print('k=', k, ', Accuracy of SVM is:', accuracy)

# plot the influence of k
x = np.arange(1, 16)
plt.plot(x, rf_results, 'bs', label='Random Forest')
plt.plot(x, svm_results, 'g^', label='SVM')

plt.xlabel('value of k for PCA')
plt.ylabel('accuracy')
plt.legend()
plt.show()

# top k for feature extraction(fe)
pca = PCA(n_components=2)
x_train_fe = pca.fit_transform(x_train_fe)
x_test_fe = pca.transform(x_test_fe)

# RF for feature extraction 
classifier = RandomForestClassifier(n_estimators=10, 
                                    criterion='entropy', 
                                    random_state=0)
classifier.fit(x_train_fe, y_train)
y_pred = classifier.predict(x_test_fe)
accuracy = accuracy_score(y_test, y_pred)
print('Accuracy of RF for feature extraction is:', accuracy)

# SVM for feature extraction
classifier = sklearn.svm.SVC(kernel='linear', 
                             C=0.1, 
                             gamma=10, 
                             probability=True)
classifier.fit(x_train_fe, y_train)
prediction = classifier.predict(x_test_fe)
accuracy = accuracy_score(prediction, y_test)
print('Accuracy of SVM for feature extraction is:', accuracy)